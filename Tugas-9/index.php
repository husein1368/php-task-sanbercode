<?php
require_once 'Class/Animal.php';
require_once 'Class/Frog.php';
require_once 'Class/Ape.php';

// Memanggil Class Animal ---------------------------
// Membuat instance objek 'sheep' dengan name "shaun"
$sheep = new Animal("shaun");

// Mengakses property name dari sheep
echo "Name : ".$sheep->name."<br>"; // "shaun"

// Mengakses property legs dari sheep
echo "Legs : ".$sheep->legs."<br>"; // 4

// Mengakses property cold_blooded dari sheep
echo "Cold Blooded : ".$sheep->cold_blooded."<br>"; // "no"

echo "<br>";
// Memanggil Class Frog -----------------------------
$kodok = new Frog("buduk");
echo "Name : ".$kodok->name."<br>"; // "buduk"
echo "Legs : ".$kodok->legs."<br>"; // 4
echo "Cold Blooded : ".$kodok->cold_blooded."<br>"; // "no"
echo "Jump : ". $kodok->jump() ."<br>";

echo "<br>";
// Memanggil Class Ape ------------------------------
$bedes = new Ape("sungokong");
echo "Name : ".$bedes->name."<br>"; // "sungokong"
$bedes->setLegs(2); // Merubah default property legs 
echo "Legs : ".$bedes->legs."<br>"; // 2
echo "Cold Blooded : ".$bedes->cold_blooded."<br>"; // "no"
echo "Yell : ".$bedes->yell()."<br>";
?>